#!/usr/bin/env python

from netaddr import *
import pprint
pp = pprint.PrettyPrinter(indent=4)
import sys
"""
Author: Charles Rumford (charlesr@isc.upenn.edu)

usage: indeling [config] ([config]...)

The purpose of this script is to take a Cisco or a Brocade configuration and
produce a mapping of router to interface to IPv4 to IPv6 addresses. The output
of this can then be used for a number of different anyalsises.

The goal is to gather the following information:
    - Router
    - Interface
    - Description
    - IPv4 addresses on the interface
    - IPv6 addresses on the interface
    - Reverse Path Checking Status

The ouput is three CSV files:
    ifaces.csv
        * router
        * interface
        * description
        * VRF
        * uRPF4
        * uRPF6
        * IPv4 helper addresses
    iface_v4.csv
        * router
        * interface
        * ipv4
    iface_v6.csv
        * router
        * interface
        * ipv6



"""

######
# TODO: Process the command line arguments
######
if len(sys.argv) < 2:
    print "Need file names"
    exit(1)

router_list=sys.argv[1:]
####
# Assuming a list of routers
####

fin = None
rtr_info = {}

"""
Information on rtr_info structure

rtr_info ->
    |- <router name>
    |   |-<interface>
    |   |   |-desc:<string>
    |   |   |-ipv4:<list of IPv4 addresses>
    |   |   |-ipv6:<list of IPv6 addresses>
    |   |   |-rfpc:True|False
    |   |-<interface>
    |   |   |-desc:<string>
    |   |   |-ipv4:<list of IPv4 addresses>
    |   |   |-ipv6:<list of IPv6 addresses>
    |   |   |-rfpc:True|False
    |- <router name>
    |   |-<interface>
    |   |   |-desc:<string>
    |   |   |-ipv4:<list of IPv4 addresses>
    |   |   |-ipv6:<list of IPv6 addresses>
    |   |   |-rfpc:True|False
    |   |-<interface>
    |   |   |-desc:<string>
    |   |   |-ipv4:<list of IPv4 addresses>
    |   |   |-ipv6:<list of IPv6 addresses>
    |   |   |-rfpc:True|False
"""
def debug(string):
    #print string
    return

#router_list = ['vagelos.router.upenn.edu','hnt1.be.router.upenn.edu']
for conf_file in router_list:
    fin = open(conf_file,"r")
    rtr_info[conf_file]={'type':None}
    rtr=rtr_info[conf_file]
    for line in fin.readlines():
        #First let grab the hostname
        line=line.strip().split()
        debug(line)
        if len(line) < 1:
            continue
            continue
        #let find our first interface
        if line[0].find('interface') == 0:
            debug("setting up "+str(line))
            int_name=" ".join(line[1:])
            rtr[int_name]={}
            iface=rtr[int_name]
            continue
        if 'shutdown' in line[0]:
            iface['shutdown']=True
            continue
        if 'enable' in line[0] and len(line) == 1:
            iface['enable']=True
            continue
        if 'disable' in line[0] and len(line) ==1:
            iface['enable']=False
        #grab the descripton
        if 'description' in line[0]:
            iface['desc'] = " ".join(line[1:])
            rtr['type']="Cisco"
            continue
        if 'port-name' in line[0]:
            iface['desc'] = " ".join(line[1:])
            rtr['type']="Brocade"
            continue
        if " ".join(line).find("ip helper-address") == 0:
            if 'ipv4-helper' not in iface.keys():
                iface['ipv4-helper']=[]
            iface['ipv4-helper'].append(line[2])
            continue
        #let's find some IP addresses
        if " ".join(line).find("ip address") == 0:
        #if 'ip' in line[0] and len(str(line[0]))==2 and len(line) > 3 and line[2].find('address') == 0:
            if 'ipv4' not in iface.keys():
                iface['ipv4']=[]
            if len(line) == 3:
                #We have a brocade
                iface['ipv4'].append(IPNetwork(line[2]))
            elif len(line) >= 4:
                iface['ipv4'].append(IPNetwork(line[2]+"/"+line[3]))
            continue
        if 'ipv6' in line[0] and 'address' in line[1]:
            if 'ipv6' not in iface.keys():
                iface['ipv6']=[]
            iface['ipv6'].append(IPNetwork(line[2]))
            continue
        if line[0].find("vrf") == 0 and line[1] == "forwarding":
            iface['vrf'] = line[2]
            continue
        #finally, reverse path checking
        if 'ip' in line[0] and 'verify' in line[1]:
            #cisco reverse path checking
            iface['ipv4_rfpc']=True
            continue
        if 'ipv6' in line[0] and 'verify' in line[1]:
            #cisco reverse path checking
            iface['ipv6_rfpc']=True
            continue
        if 'reverse-path-check' in line[0]:
            #brocade reverse path checking
            iface['ipv4_rfpc']=True
            continue
        if 'ipv6' in line[0] and 'reverse-path-check' in line[1]:
            #brocade reverse path checking
            iface['ipv6_rfpc']=True
            continue

def quo(string):
    return string
    return "\""+string+"\""

iface_out=open("iface.csv","w")
v4_out=open("iface_v4.csv","w")
v6_out=open("iface_v6.csv","w")

routers=rtr_info.keys()
routers.sort()
for rtr_name in routers:
    rtr=rtr_info[rtr_name]
    ifaces=rtr.keys()
    ifaces.sort()
    for iface in ifaces:
        if iface == 'type':
            continue
        if 'ipv6' not in rtr[iface].keys() and 'ipv4' not in rtr[iface].keys():
            continue
        key=iface_line= quo(rtr_name)+","+quo(iface)+","
        v4_line=v6_line=""
        if "desc" in rtr[iface].keys():
            iface_line+=quo(rtr[iface]['desc'].replace(',',' '))+","
        else:
            iface_line+=quo("")+","
        if rtr['type'] == "Cisco":
            if 'shutdown' in rtr[iface].keys():
                iface_line+=quo("Admin Down")+","
            else:
                iface_line+=quo("Admin Up")+","
        elif rtr['type'] == "Brocade":
            if 'enable' in rtr[iface].keys():
                if rtr[iface]['enable']:
                    iface_line+=quo("Admin Up")+","
                else: 
                    iface_line+=quo("Admin Down")+","
            else:
                iface_line+=quo("Admin Up")+","
        if 'vrf' in rtr[iface].keys():
            iface_line+=quo(rtr[iface]['vrf'])+","
        else:
            iface_line+=quo("")+","
       
        if 'ipv4' in rtr[iface].keys():
            v4=""
            for ipv4 in rtr[iface]['ipv4']:
                v4_line+=key+str(ipv4.cidr)+"\n"
        
        if 'ipv4_rfpc' in rtr[iface].keys():
            iface_line+=quo("Yes")+","
        else:
            iface_line+=quo("No")+","
        if 'ipv4-helper' in rtr[iface].keys():
            v4=""
            for ipv4 in rtr[iface]['ipv4-helper']:
                v4=v4+" "+ipv4
            iface_line+=quo(v4.strip())+","
        else:
            iface_line+=quo("")+","

        if 'ipv6' in rtr[iface].keys():
            v6=""
            for ipv6 in rtr[iface]['ipv6']:
                v6_line+=key+str(ipv6.cidr)+"\n"

        if 'ipv6_rfpc' in rtr[iface].keys():
            iface_line+=quo("Yes")
        else:
            iface_line+=quo("No")

        iface_out.write(iface_line+"\n")
        v4_out.write(v4_line)
        v6_out.write(v6_line)

iface_out.close()
v4_out.close()
v6_out.close()
