from pysnmp.entity.rfc3413.oneliner import cmdgen

cmdGen = cmdgen.CommandGenerator()
cmdGen.ignoreNonIncreasingOid = True
errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.getCmd(
        cmdgen.UsmUserData('testtest', 'testtest123', 'testtest123','authPriv'),
        cmdgen.UdpTransportTarget(('128.91.240.90', 161)),
        cmdgen.MibVariable('WLSX-AUTH-MIB','').addMibSource('./mibs').loadMibs('WLSX-AUTH-MIB')
        )
if errorIndication:
    print(errorIndication)
else:
    if errorStatus:
        print('%s at %s' % (
            errorStatus.prettyPrint(),
            errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
            )
        )
    else:
        for varBindTableRow in varBindTable:
            for name, val in varBindTableRow:
                print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
