#!/usr/bin/perl -w 
#
use strict;

my $command = "/usr/local/bin/eapol_test";
my $base_dir = "/home/charlesr/code/testing/";
my $mailto = 'charlesr@drexel.edu,m@drexel.edu';

my %servers=(
    "entropy" => [ "144.118.38.122", "IMsSGNTaDehN"],
   "speakeasy" => [ "144.118.38.6", "IMsSGNTaDehN"],
    "avocet" => [ "144.118.38.206", "7Ded7bNWHnV0"],
     "grebe" => [ "144.118.38.205", "7Ded7bNWHnV0"] );

my %auths=(
    "peap" => $base_dir."conf/peap.conf",
    "ttls-mschapv2" => $base_dir."conf/ttls-mschap.conf",
    "ttls-pap" => $base_dir."conf/ttls-pap.conf");

sub test_radius {
    my $server = shift(@_);
    my $key = shift(@_);
    my $conf = shift(@_);

    my $output = `${command} -c ${conf} -a ${server} -s ${key} 2&>1 > /dev/null`;

    if ( $? != 0 ) {
        return 1;
    } else { 
        return 0;
    }
}

my $error_log = "";

while ( my ($ser, $ser_details) = each(%servers) ) {
    while ( my ($auth, $conf) = each(%auths) ) {
        if ( test_radius(@$ser_details[0], @$ser_details[1], $conf) ) {
            $error_log .= "TROUBLE ON $ser USING $auth\n";
        }
    }
}

if (not $error_log eq "") {
    my $date = `date`;
    chomp($date);
    my $subject = "Troubles with RADIUS Auth (".$date.")";
    open MAIL, "| mail -s \"$subject\" $mailto";
    print MAIL $error_log;
    close MAIL;
} 

