#!/usr/bin/perl
#
use strict;
use Net::SNMP qw(:snmp);
use Data::Dumper;


my ($session, $error) = Net::SNMP->session(
      -hostname     => '10.50.80.68',
      -version      => 'snmpv3',
      -username     => 'autoprovision',
      -authprotocol => 'md5',
      -authpassword => 'foobar123', 
      -privprotocol => 'des',
      -privpassword => '123foobar'
   );

   if (!defined $session) {
      printf "ERROR: %s.\n", $error;
      exit 1;
   }


my $base_oid = '1.3.6.1.4.1.14823.2.2.1.5.2.1.4.1';

my %entries = (
    wlanAPMacAddress    => "${base_oid}.1",
    wlanAPIpAddress     => "${base_oid}.2",
    wlanAPName          => "${base_oid}.3",
    wlanAPGroupName     => "${base_oid}.4",
    wlanAPSerialNumber  => "${base_oid}.6");


my @cols = qw(
    wlanAPMacAddress 
    wlanAPIpAddress  
    wlanAPName       
    wlanAPGroupName  
    wlanAPSerialNumber
    );


my $result = $session->get_entries (
    -columns => [ map { $entries{$_} } @cols ]);

   if (!defined $result) {
      printf "ERROR: %s\n", $session->error();
      $session->close();
      exit 1;
   }
    print "Keys of \$result:\n";
my %aps = ();
foreach my $key ( keys %{$result} ) {
    my $value = $result->{$key};
   
    my ($id, $index) = ($key =~ m/^$base_oid.(.).(.+)$/);
    
    if (not exists $aps{$index}) {
        $aps{$index} = ();
    }
    $aps{$index}{$id}=$value;
}

print Dumper(%aps);

foreach my $key ( keys %aps ) {
    if($aps{$key}{'4'} =~ m/default/) {
        printf("%s\t\t%s\n",$aps{$key}{'2'},$aps{$key}{'4'});
    }
}
   # Now initiate the SNMP message exchange.

   #snmp_dispatcher();

   $session->close();




