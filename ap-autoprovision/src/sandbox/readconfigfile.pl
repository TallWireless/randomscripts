use strict;
use warnings;


use Config::IniFiles;

my $file;

if (@ARGV) {
    $file = $ARGV[0];
} else {
    die 'Need a file!';
}

my $cfg = Config::IniFiles->new( -file => $file);


foreach my $section ($cfg->Sections()) {
    foreach my $parameter ($cfg->Parameters($section)) {
        printf("%s->%s->%s\n",$section,$parameter,$cfg->val($section,$parameter));
    }
}
