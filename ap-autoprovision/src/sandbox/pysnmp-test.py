#/usr/bin/python2

"""
This is just for playing around with the NET-SNMP modules for python.

Goals are to just be able to look at the AP table from a controller.

"""


from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.smi import builder
cmdGen = cmdgen.CommandGenerator()

"""
errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.bulkCmd(
    cmdgen.UsmUserData(
            userName = 'autoprovision',
            authKey  = 'foobar123',
            privKey  = '123foobar'),
    cmdgen.UdpTransportTarget(('10.50.80.68', 162)),
    0, 50,
    cmdgen.MibVariable('IP-MIB', 'ipAdEntAddr').addMibSource('/tmp/mib'),
    lexicographicMode=True, maxRows=20, ignoreNonIncreasingOid=True
)
"""

errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.bulkCmd(
    cmdgen.UsmUserData(userName = 'autoprovision',
            authKey  = 'foobar123',
            privKey  = '123foobar',
            authProtocol = cmdgen.usmHMACMD5AuthProtocol,
            privProtocol = cmdgen.usmDESPrivProtocol),
    cmdgen.UdpTransportTarget(("127.0.0.1", 161)),
    1, 25,
    cmdgen.MibVariable('IP-MIB',
        'ipAdEntAddr').addMibSource('/tmp/mib'),
    cmdgen.MibVariable('IF-MIB', 'ifEntry').addMibSource('/tmp/mib'),
    lookupNames=True, lookupValues=True, maxRows=20 )
"""
if errorIndication:
    print(errorIndication)
else:
    if errorStatus:
        print('%s at %s' % (
            errorStatus.prettyPrint(),
            errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
            )
        )
    else:
        for varBindTableRow in varBindTable:
            for name, val in varBindTableRow:
                print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
                """
