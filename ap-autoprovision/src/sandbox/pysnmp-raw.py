from pysnmp.entity.rfc3413.oneliner import cmdgen

cmdGen = cmdgen.CommandGenerator()

errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
    cmdgen.UsmUserData('usr-sha-aes128', 'authkey1', 'privkey1',
                authProtocol = cmdgen.usmHMACMD5AuthProtocol,
                privProtocol = cmdgen.usmDESPrivProtocol),
    cmdgen.UdpTransportTarget(('127.0.0.1', 161)),
    cmdgen.MibVariable('IF-MIB',
        '').addMibSource('/Users/charlesr/Code/pysnmp_mibs/snmp-mibs'),
    lexicographicMode=True, maxRows=100,
    ignoreNonIncreasingOid=True
)

if errorIndication:
    print(errorIndication)
else:
    if errorStatus:
        print('%s at %s' % (
            errorStatus.prettyPrint(),
            errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
            )
        )
    else:
        for varBindTableRow in varBindTable:
            for name, val in varBindTableRow:
                print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
