#!/usr/bin/perl -w
#
# Author:   Charles Rumford (charlesr@isc.upenn.edu)
#           University of Penn
#           ISC/Tech Services/Network Operations
#
#
# About: Goes out and collects information about the stats for a group of
# controllers defined in the config file and outputs then in munin format.
#

## 20151030 TODO:
#   - Add the summation graph as a subgraph of it self
#   - Grab the avgerage response times and start grapthing them
#       - How to handle the averages over a collection of controllers
#       - possible issues with controllers that only have one or two requests
#         messing up the collective avgerage.


use strict;
use warnings;

use Munin::Plugin;

my $debug = '';
sub debug {
    my $line = shift;

    print $line if $debug;
}

use Data::Dumper;

# For getting stats table from controller
use Net::SNMP qw(:snmp);

# for the config files
use Config::IniFiles;

# Some handy references
my $authAddress = 3;
my $authSuccess = 10;
my $authFail = 11;
my $authTimeout = 12;


#variables
my $config_file = "config.ini";
my $host = 'wlan.upenn.edu';

# Setup config::inifile tie
my $conf = Config::IniFiles->new( -file => $config_file );

if (not defined $conf) {
    die "problems with dealing with the config file";
}

sub get_stats_table {
    
    my $ctrl = shift;
#    debug("opening SNMP session to $ctrl\n");
#    debug("SNMP Host: \"$ctrl\"\n");
    my ($session, $error) = Net::SNMP->session(
        -hostname     => $ctrl."",
        -version      => 'snmpv3',
        -username     => $conf->val('snmp','user')."",
        -authprotocol => 'md5',
        -authpassword => $conf->val('snmp','auth_pw')."",
        -privprotocol => 'des',
        -privpassword => $conf->val('snmp','priv_pw').""
    );

    if (!defined $session) {
        die "ERROR: unable to contact $ctrl\nreason: $error"
    }

    my $base_oid = '1.3.6.1.4.1.14823.2.2.1.8.1.1.1';

    my %entries = (
        authServerAddress => "${base_oid}.3",
        authServerSuccessfulAuths => "${base_oid}.10",
        authServerFailedAuths => "${base_oid}.11",
        authServerTimeouts => "${base_oid}.12"
    );


    my @cols = qw(
        authServerAddress
        authServerSuccessfulAuths
        authServerFailedAuths
        authServerTimeouts
    );

    debug("fetching the table of importance...\n");
    my $result = $session->get_entries (
        -columns => [ map { $entries{$_} } @cols ]);

    if (!defined $result) {
        printf "ERROR: %s\n", $session->error();
        $session->close();
        exit 1;
    }
    
    debug("cleaning up the table output...\n");

    my %aps = ();
    foreach my $key ( keys %{$result} ) {
        my $value = $result->{$key};

        my ($id, $index) = ($key =~ m/^$base_oid\.([0-9]*)\.(.+)$/);
        debug("\$id is \"$id\" and \$index is \"$index\"\n");
        if (not exists $aps{$index}) {
            $aps{$index} = ();
        }
        $aps{$index}{$id}=$value;
    }
    return %aps;
}


sub find_radius {
    my $srv = shift;
    foreach ($conf->Parameters('radius')) {
        if ( $_ eq $srv ) {
            return 1;
        } 
    }
    return 0;
}
# verify default and master controllers are defined correctly

$conf->SectionExists('snmp') || 
    die "Missing \'[ snmp ]\' from the configuration";

$conf->SectionExists('controllers') || 
    die "Missing \'[ controllers ]\' from the configuration";


if ( $ARGV[0] and $ARGV[0] eq "config" ) {
        
    foreach  ($conf->Parameters("radius")) {
        my $server  = $conf->val('radius',$_);
        print "multigraph ".clean_fieldname("${server}auths")."\n";
        print "graph_title Auth Stats  $server\n";
        print "graph_args --lower-limit 0 --rigid\n";
        print "graph_category wireless\n";
        #print "graph_order sum_auth_accept sum_auth_fail sum_auth_timeout\n";
        print "graph_total Total\n";
        print "graph_vtitle Auths\n";
        print "sumauthfail.label Sum fail\n";
        print "sumauthfail.draw AREASTACK\n";
        print "sumauthfail.type DERIVE\n";
	print "sumauthfail.warning :5\n";
	print "sumauthfail.critical :10\n";
        print "sumauthtimeout.label Sum timeout\n";
        print "sumauthtimeout.draw AREASTACK\n";
        print "sumauthtimeout.type DERIVE\n";
	print "sumauthtimeout.warning :1\n";
	print "sumauthtimeout.critical :2\n";
        print "sumauthaccept.label Sum Accept\n";
        print "sumauthaccept.draw AREASTACK\n";
        print "sumauthaccept.type DERIVE\n";

        ## children nodes
        foreach my $controller ( $conf->Parameters('controllers')) {
            print "\n";
            print "multigraph ".clean_fieldname("${server}auths").".".clean_fieldname($controller)."\n";
            print "graph_title Auth Stats  $server  $controller\n";
            print "graph_category wireless\n";
            print "graph_order auth_accept auth_fail auth_timeout\n";
            print "graph_total Total\n";
            print "graph_args --lower-limit 0 --rigid \n";
            print "graph_vtitle Auths\n";
#            print $controller."_".$server."_auth_fail.draw AREASTACK\n";
#            print $controller."_".$server."_auth_fail.type DERIVE\n";
#            print $controller."_".$server."_auth_fail.label Fail\n";
#            print $controller."_".$server."_auth_timeout.draw AREASTACK\n";
#            print $controller."_".$server."_auth_timeout.type DERIVE\n";
#            print $controller."_".$server."_auth_timeout.label Timeout\n";
#            print $controller."_".$server."_auth_accept.draw AREASTACK\n";
#            print $controller."_".$server."_auth_accept.type DERIVE\n";
#            print $controller."_".$server."_auth_accept.label Accept\n";
            print "authfail.label Fail\n";
            print "authfail.draw AREASTACK\n";
            print "authfail.type DERIVE\n";
	    print "authfail.warning :5\n";
	    print "authfail.critical :10\n";
            print "authtimeout.label Timeout\n";
            print "authtimeout.draw AREASTACK\n";
            print "authtimeout.type DERIVE\n";
            print "authaccept.label Accept\n";
            print "authaccept.draw AREASTACK\n";
            print "authaccept.type DERIVE\n";
        }
        print "\n";
    }
    exit(0);
}

my %data = ();

foreach my $radius ( $conf->Parameters('radius') ) {
    $data{$radius} = {};
    $data{$radius}{'TOTALSUM'} = {};
    $data{$radius}{'TOTALSUM'}{'ACCEPTED'} = 0;
    $data{$radius}{'TOTALSUM'}{'FAILED'} = 0;
    $data{$radius}{'TOTALSUM'}{'TIMEOUT'} = 0;
}

my @radius_servers = $conf->Parameters('radius');

foreach my $controller ( $conf->Parameters('controllers') ) {
    my %table = get_stats_table($conf->val('controllers',$controller));
    foreach my $index (keys %table) {

        if ( find_radius($table{$index}{$authAddress}) ){
            my $radius = $table{$index}{$authAddress};
            debug "Handling $radius for $controller\n";
            $data{$radius}{$controller} = {};
            $data{$radius}{$controller}{'ACCEPTED'} = $table{$index}{$authSuccess};
            $data{$radius}{'TOTALSUM'}{'ACCEPTED'} += $table{$index}{$authSuccess};
            $data{$radius}{$controller}{'FAILED'} = $table{$index}{$authFail};
            $data{$radius}{'TOTALSUM'}{'FAILED'} += $table{$index}{$authFail};
            $data{$radius}{$controller}{'TIMEOUT'} = $table{$index}{$authTimeout};
            $data{$radius}{'TOTALSUM'}{'TIMEOUT'} += $table{$index}{$authTimeout};
        }
    }
}

#printout the totalsums graph first, then the controllers

foreach my $radius (keys %data)  {
    my $server  = $conf->val('radius',$radius);
    print "multigraph ".clean_fieldname("${server}auths")."\n";
    print "sumauthaccept.value $data{$radius}{'TOTALSUM'}{'ACCEPTED'}\n";
    print "sumauthfail.value $data{$radius}{'TOTALSUM'}{'FAILED'}\n";
    print "sumauthtimeout.value $data{$radius}{'TOTALSUM'}{'TIMEOUT'}\n";

    foreach my $controller (keys %{$data{$radius}} ) {
        if ($controller eq 'TOTALSUM') {
            next;
        }
        print "\n";
        print "multigraph ".clean_fieldname("${server}auths").".".clean_fieldname($controller)."\n";
#        print $controller."_".$server."_auth_accept.value $data{$radius}{$controller}{'ACCEPTED'}\n";
#        print $controller."_".$server."_auth_failed.value $data{$radius}{$controller}{'FAILED'}\n";
#        print $controller."_".$server."_auth_timeout.value $data{$radius}{$controller}{'TIMEOUT'}\n";
        print "authaccept.value $data{$radius}{$controller}{'ACCEPTED'}\n";
        print "authfail.value $data{$radius}{$controller}{'FAILED'}\n";
        print "authtimeout.value $data{$radius}{$controller}{'TIMEOUT'}\n";

    }
    print "\n\n";
}

