#!/usr/bin/perl -w

use Data::Dumper;

# For getting AP table from controller
use Net::SNMP qw(:snmp);

sub get_ap_table {
    my $config = shift;
    my $base_oid = shift;
    my $entries = shift;
    my $cols = shift;
    my $index = shift;

    print("\$config: $config\n");
    print Dumper(%$config);
    print("\$base_oid: $base_oid\n");
    print Dumper($base_oid);
    print("\$entries: $entries\n");
    print Dumper(%$entries);
    print("\$cols: $cols\n");
    print Dumper(@$cols);

    my ($session, $error) = Net::SNMP->session(
        -hostname     => %$config{'hostname'},
        -version      => 'snmpv3',
        -username     => %$config{'username'},
        -authprotocol => 'md5',
        -authpassword => %$config{'authpw'},
        -privprotocol => 'des',
        -privpassword => %$config{'prvipw'},
    );

    if (!defined $session) {
        die "ERROR: unable to contact ".$conf->val('controller default','ip')."\nreason: $error"
    }
    
    debug("fetching the table of importance...\n");
    my $result = $session->get_entries (
        -columns => [ map { $$entries{$_} } @$cols ]);

    if (!defined $result) {
        printf "ERROR: %s\n", $session->error();
        $session->close();
        exit 1;
    }
    
    debug("cleaning up the table output...\n");

    my %temp = ();


    #
    # The output of this function should be a hash with the index being
    # defined by $index. This allows the programmer to do more revent things
    # with the data.
    #
    #
    # First up, gather like SNMP indenices together.

    foreach my $key ( keys %{$result} ) {
        my $value = $result->{$key};

        # Need to fix this so the regex is more generic and matches on
        # everything
        # Not cool.
        
        my ($id, $index) = ($key =~ m/^$base_oid.(3|2|4|19).(.+)$/);

        if (not exists $temp{$index}) {
            $temp{$index} = ();
        }
        $temp{$index}{$id}=$value;

    }

    # Second up, setup the hash in the form needed for output!
    
    my %return = ();
    
    foreach my $key ( keys %temp ) {
        my $new_key = $temp{$key}{$index};
        $return{$new_key} = ();
        foreach (keys $temp{$key}) {
            $return{$new_key}{$_} = $temp{$key}{$_};
        }
    }

    return %return;
}

    my $base_oid = "1.3.6.1.4.1.14823.2.2.1.5.2.1.7.1";
    my %entries = (
        wlanAPBSSID                     => "${base_oid}.1",
        wlanAPESSID                     => "${base_oid}.2",
        wlanAPBssidPhyType              => "${base_oid}.5",
        wlanAPBssidAPMacAddress         => "${base_oid}.13"
    );


    my @cols = qw(
        wlanAPBSSID             
        wlanAPESSID             
        wlanAPBssidPhyType      
        wlanAPBssidAPMacAddress 
    );
    
    my %config = ( hostname => '10.50.80.68',
                   username => 'autoprovision',
                   authpw   => 'foobar123',
                   privpw   => '123foobar' );

    my $index = '13';

get_ap_table(\%config, $base_oid, \%entries, \@cols);
