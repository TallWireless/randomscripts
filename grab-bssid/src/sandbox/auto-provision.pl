#!/usr/bin/perl -w
#
# Author:   Charles Rumford (charlesr@isc.upenn.edu)
#           University of Penn
#           ISC/Tech Services/Network Operations
#
# About: This is a script to go out and poll the default controller, figure
# out what APs are sitting in the default group, and then reprovision them
# based on its IP address.
#
#
# USAGE: auto-provision.pl [--noop] [--debug] --config config_file
#       
#       --noop -> go out and fetch the AP table, and figure out who needs to
#                 needs to be regrouped and print out the commands to be sent
#                 to the master controller
#
#       --debug -> well, turn on debuging output and the amount is at the whim
#                  of the programmer
#       
#       --config -> the configuration file for the session
#       
#       --mail -> email given people a report of what has happened or would
#                 happen
#
# TODO: Add email option for emailing reports of when APs are moved
#       Possible as an option in the config file? along with the ability to
#       list out email addresses
use strict;
use warnings;

my $debug = '';
sub debug {
    my $line = shift;

    print $line if $debug;
}

use Data::Dumper;

# For getting AP table from controller
use Net::SNMP qw(:snmp);

# For reprovisioning APs via the master controller
use Net::SSH::Expect;

# Used for reading the config file
use Config::IniFiles;

# Handle command line arguments
use Getopt::Long;

# For handling IP addressing
use Net::CIDR::Lite;

# Some handy references
my $AP_MAC_ADDRESS = '1';
my $AP_IP_ADDRESS = '2';
my $AP_NAME = '3';
my $AP_GROUP = '4';
my $AP_SERIAL_NUM = '6';
my $AP_STATUS = '19';


# setup and verify cli arguments

my $noop = '';
my $config_file = "";

GetOptions( "config=s"  => \$config_file,
            "debug"     => \$debug,
            "noop"      => \$noop) 
        || die("You are missing your config file. Need --config");

debug("\$debug is \"$debug\"\n");
debug("\$noop is \"$noop\"\n");
debug("\$config_file is \"$config_file\"\n");

# Setup config::inifile tie
my $conf = Config::IniFiles->new( -file => $config_file );
if (not defined $conf) {
    die "problems with dealing with the config file";
}

sub email_report {
    my %aps = @_;
    debug("Let's write an email report to the configured email addresses...");
    my $date = `date +\%Y\%m\%d-\%H\%M`;
    my $sendto = $cfg->val('general','mailto');
    open MAIL, "|/bin/mail -s 'AP re-grouping report ($date)' ".$cfg->val('general','mailto');
    print MAIL "All - \n";
    if ($noop) {
        print MAIL "The following APs would have been regrouped:";
    } else {
        print MAIL "The following APs were regrouped: \n\n";
    }
    foreach my $ap (%aps) {
        print MAIL "   ".$aps{$ap}{$AP_NAME};
        print MAIL "\t".$aps{$ap}{$AP_GROUP}."->".$aps{$ap}{$newGroup};
        print MAIL "\t".$aps{$ap}{$AP_IP_ADDRESS};
        print MAIL "\n";
    }

    print MAIL "\n\n";
    print MAIL "That is all.\n";
    print MAIL "THIS IS AN AUTO GENERATED MESSAGE.\n"
    close(MAIL);

    debug("message sent");

}


sub get_ap_table {
    debug("opening SNMP session to ".$conf->val('controller default','ip')."\n");
    debug("SNMP Host: \"".$conf->val('controller default','ip')."\"\n");
    debug("SNMP Username: \"".$conf->val('controller default','snmp_username')."\"\n");
    my ($session, $error) = Net::SNMP->session(
        -hostname     => "".$conf->val('controller default','ip'),
        -version      => 'snmpv3',
        -username     => $conf->val('controller default','snmp_username')."",
        -authprotocol => 'md5',
        -authpassword => $conf->val('controller default','snmp_auth_pw')."",
        -privprotocol => 'des',
        -privpassword => $conf->val('controller default','snmp_priv_pw').""
    );

    if (!defined $session) {
        die "ERROR: unable to contact ".$conf->val('controller default','ip')."\nreason: $error"
    }

    my $base_oid = '1.3.6.1.4.1.14823.2.2.1.5.2.1.4.1';

    my %entries = (
        wlanAPIpAddress     => "${base_oid}.2",
        wlanAPName          => "${base_oid}.3",
        wlanAPGroupName     => "${base_oid}.4",
        wlanAPStatus        => "${base_oid}.19"
    );


    my @cols = qw(
        wlanAPIpAddress  
        wlanAPName       
        wlanAPGroupName  
        wlanAPStatus
    );

    debug("fetching the table of importance...\n");
    my $result = $session->get_entries (
        -columns => [ map { $entries{$_} } @cols ]);

    if (!defined $result) {
        printf "ERROR: %s\n", $session->error();
        $session->close();
        exit 1;
    }
    
    debug("cleaning up the table output...\n");

    my %aps = ();
    foreach my $key ( keys %{$result} ) {
        my $value = $result->{$key};

        my ($id, $index) = ($key =~ m/^$base_oid.(3|2|4|19).(.+)$/);

        if (not exists $aps{$index}) {
            $aps{$index} = ();
        }
        $aps{$index}{$id}=$value;
    }
    return %aps;
}

sub get_new_ap_group { 
    my %aps = @_;
    debug("Shall we gather some new AP groups...\n");
    foreach my $subnet ($conf->Parameters('ap-groups')) {
       my $cidr = Net::CIDR::Lite->new;
       debug("Checking APs for subnet $subnet...\n");
       $cidr->add($subnet);
       foreach my $ap (keys %aps) {
           debug("    Checking AP ".$aps{$ap}{$AP_NAME}." with ip address ".$aps{$ap}{$AP_IP_ADDRESS}."...\n");
           if ($cidr->prep_find("".$aps{$ap}{$AP_IP_ADDRESS})) {
               debug("    Found a new group:".
                   $conf->val('ap-groups',$subnet)." \n");
               $aps{$ap}{'newGroup'} = $conf->val('ap-groups',$subnet);
            }
        }
    }

    return %aps;
}

sub reprovision_aps {
    debug("Time to reprovision aps...\n");
    my %aps = @_;

    # TODO: add ability to use an SSH key to SSH into the master controller
    #       ssh_private_key: /some/file/path needs to be added to the config
    #       file
    my $ssh = Net::SSH::Expect->new (
        host        => $conf->val('controller master','ip'),
        user        => $conf->val('controller master','ssh_username'),
        password    => $conf->val('controller master', 'ssh_password'),
        raw_pty     => 1,
        log_file    => '/tmp/ssh_log_file'
    );
    
    # start up the ssh session
    if ( not $noop ) {
        $ssh->run_ssh() or die "Unable to SSH into the master controller: $!";
        $ssh->login();
        debug("SSH Session has been started to the master controller...\n");
    } else {
        print "NOOP: SSH Start";
    }
    
    # get our selves into enable
    if ( not $noop ) {
        debug("Shall we enter enable mode...");
        #$ssh->waitfor('.* >.*') or die "SSH problems: Enable failed";
        $ssh->send('enable');
        #$ssh->waitfor('Password:') or die "SSH problems: Enable Failed";
        $ssh->send($conf->val('controller master','ssh_password'));
        #$ssh->waitfor('.*\) #.*') or die "SSH Problems: Enable Failded";
        debug("I think we shall...\n");
    } else {
        print "NOOP: SSH Enable here\n";
    }

    # Ok...Now to actually regroup the aps...
    foreach my $ap (keys %aps) {
        debug("Examing ap ".$aps{$ap}{$AP_NAME}."...");
        if ( exists $aps{$ap}{'newGroup'} ) {
            my $command = "ap-regroup ap-name $aps{$ap}{$AP_NAME} $aps{$ap}{'newGroup'}";
            if ( $noop ) {
                print "NOOP: command to master: $command\n"
            } else {
                debug("Sending command \"$command\" to master...\n");
                $ssh->send($command);
                #$ssh->waitfor('.*\) #.*') or die "SSH Problems: Failded waiting for prompt";
                debug("Command Complete.\n");
            }
        }
    }

    # close ssh session
    if (not $noop) {
        $ssh->send("exit");
        $ssh->send("exit");
        $ssh->close();
    }
}




# verify default and master controllers are defined correctly
#TODO: Clean this up and have more proper checking for deafault and master
my @required_parameters = qw(
    ip);

foreach my $member ($conf->GroupMembers('controller')) {
    foreach my $req_para (@required_parameters) {
        debug("Checking \'$member\' for \'$req_para\'...\n");
        defined $conf->val($member,$req_para) || die ("missing $req_para for $member");
    }
}

# verify that the ap-groups section exists

$conf->SectionExists('ap-groups') || 
    die "Missing \'[ ap-groups ]\' from the configuration";

##########
#  Let's do some real work now
##########

my %ap_table = get_ap_table();

# clean up the table of uneeded entries
# TODO: add check to make sure the AP is actually up!

foreach my $ap (keys %ap_table) {
    debug("Checking $ap in ".$ap_table{$ap}{$AP_GROUP}." in ".
        "default group...");
    if (not $ap_table{$ap}{$AP_GROUP}.""  eq "default") {
        debug("deleting entry...");
        delete $ap_table{$ap};
    }
    debug("done\n");
}
%ap_table = get_new_ap_group(%ap_table);
# Go out and reprovision the APS

my @keys = keys %ap_table;
my $size = @keys;

if ( ! $size == 0) {
    reprovision_aps(%ap_table);
}


